/* SPDX-License Identifier: GPL-3.0-or-later */

/* 
 * Source file for zrpc programming language compiler.
 * Copyright (C) 2024 Zander B. Wells

 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <https://www.gnu.org/licenses/>.

 * zbwells <zbwells@protonmail.com>
 * Written as UVA Wise Honors program research project.
 */

/* This file contains functions that handle the commenting and
 * macro functionality of the language.
 */
 
use std::collections::HashMap;

/* Remove comments from strings before processing macros */
fn remove_spaces(src: &str) -> String {
    let mut retstr = String::new();
    for line in src.lines() {
        if !line.starts_with(';') {
            retstr.push_str(&format!("{}\n", line));
        } else {
            retstr.push_str("\n");
        }
    }
    
    retstr
}
    
        
/* Deals with all of the text replacement macros */
pub fn process_macros(src: &str) -> String {
    let mut macros : HashMap<String, String> = HashMap::new();
    let mut currentmacro = String::new();
    let mut body_without_macros = String::new();
    
    /* populate the macros hashmap */
    for word in remove_spaces(src).split_whitespace() {
        if word.starts_with('#') { 
            currentmacro = (&word[1..]).to_string();
            macros.insert(currentmacro.clone(), (&"").to_string());
        } else if currentmacro == "" {
            body_without_macros.push_str(&format!(" {}", word));
            continue
        } else if word.starts_with(':') {
            currentmacro = String::from("");
        } else if word.starts_with('@') {
            macros.insert(
                currentmacro.clone(),
                format!(
                    "{} {}", 
                    macros[&currentmacro], 
                    macros[&word[1..]],
                ),
            );
        } else {
            macros.insert(
                currentmacro.clone(),
                format!(
                    "{} {}",
                    macros[&currentmacro],
                    word,
                ),
            );
        }    
    }
    
    /* use the macro hashmap to do text replacement in source */
    let mut newbody = String::new();
    for word in body_without_macros.split_whitespace() {
        if word.starts_with('@') {
            newbody.push_str(&macros[&word[1..]]);
        } else if word.starts_with(':') {
            newbody.push_str(&format!(" {}\n", word));
        } else {
            newbody.push_str(&format!(" {}", word));
        }
    }
    
    newbody
}

