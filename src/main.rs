/* SPDX-License Identifier: GPL-3.0-or-later */

/* 
 * Source file for zrpc programming language compiler.
 * Copyright (C) 2024 Zander B. Wells

 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <https://www.gnu.org/licenses/>.

 * zbwells <zbwells@protonmail.com>
 * Written as UVA Wise Honors program research project.
 */
 
/* Main parser and code generation functionality for language. */

pub mod macros;
pub mod functions;

use std::env;
use std::io::Read;
use std::io::Write;
use std::process::Command;
use std::fs::{File, metadata};
use macros::process_macros;
use std::collections::HashMap;
use functions::{get_builtins, push_onto_stack};

/* generate the intermediate C code */
fn codegen(fnbody: &str, subprogs: HashMap<String, u8>) -> String {
    let mut stackptr : usize = 0;
    let mut nest_levels = 0;
    let mut fncode = String::new();
    let mut nulls = Vec::new();
    let mut malloc_objs : Vec<usize> = Vec::new();
    let mut in_string = false;
    let builtins = get_builtins();
        
    for word in fnbody.split_whitespace() {
        /* If nesting, add spaces */
        if nest_levels > 0 && 
           !word.ends_with('}') && 
           (word != "pop" && word != "unpop") &&
           !in_string {
            for _ in 0..nest_levels {  
                fncode.push_str("  ");
            }
        }
        
        /* If user-defined function */
        if word.len() > 1 && subprogs.contains_key(word) { 
            let fname = word;
            let mut args = String::new();
            
            /* If void procedure */
            if subprogs[fname] < 1 {
                fncode.push_str(&format!(
                    "  stack[{}] = {}();\n", 
                    stackptr,
                    fname
                ));
                
                stackptr += 1;
                continue;
            }
            
            for i in (stackptr - subprogs[fname] as usize)..stackptr {
                args.push_str(&format!("stack[{}], ", i));
            }
            
            stackptr -= subprogs[fname] as usize;
            fncode.push_str(&format!(
                "  stack[{}] = {}({});\n", 
                stackptr,
                fname,
                &args[..args.len()-2],
            ));
            
            stackptr += 1;
        }
        
        /* String parsing */
        else if (word.len() > 0 && word.starts_with('"')) || in_string {
            if !in_string {
                fncode.push_str(&format!(
                    "  stack[{}].s = {}",
                    stackptr,
                    word,
                ));
                
                if word.ends_with('"') && word.len() > 1 {
                    fncode.push_str(";\n");
                    stackptr += 1;
                    in_string = false;
                    continue;
                }
                
                in_string = true;
            } else if word.ends_with('"') {
                in_string = false;
                fncode.push_str(&format!(" {};\n", word));
                stackptr += 1;
            } else {
                fncode.push_str(&format!(" {}", word));
            }
        }
            
        /* Builtin parsing */
        else if builtins.contains_key(word) {
            
            /* Secondary functions of builtins defined here: */
            if word == "c" {
                stackptr = 0;
                continue;
            } else if word == "pop" {
                stackptr = match stackptr.checked_sub(1){
                    Some(num) => num,
                    None => panic!(
                        "{}: Too few args on stack.", 
                        word
                    ),
                };
                continue;
            } else if word == "unpop" {
                stackptr += 1;
                continue;
            } else if word == "null" {
                nulls.push(stackptr);
            } else if word == "popall" {
                stackptr = nulls[0];
                nulls = Vec::new();
            } else if word == "getstr" {
                malloc_objs.push(stackptr);
            } else if word.ends_with('{') {
                nest_levels += 1;
            } else if word.ends_with('}') {
                nest_levels -= 1;
            }
            
            stackptr = match stackptr.checked_sub(
            builtins[word].arity as usize) {
                Some(num) => num,
                None => panic!(
                    "{}: Too few args on stack.", 
                    word
                ),
            };
            
            fncode.push_str(
                &builtins[word].code.replace(
                    "{count}", 
                    stackptr.to_string().as_str()
                )
            );
            
            if word == "%popstr" || word == "%prnstrs" {
                stackptr = nulls.pop().unwrap()+1;
                continue;
            }
            
            fncode.push_str("\n");
            stackptr += builtins[word].incptr as usize;
        } 
        
        /* Everything else parsing (nums, args, etc) */
        else {
            fncode.push_str(
                &push_onto_stack(word).replace(
                    "{count}",
                    stackptr.to_string().as_str()
                )
            );
            
            fncode.push_str("\n");
            stackptr += 1;
        }
    }
    
    /* Free dyn memory here (just strings, no dyn arrays of nums yet) */
    while malloc_objs.len() > 0 {
        fncode.push_str(
            &format!(
                "  free(stack[{}].s);\n",
                malloc_objs.pop().unwrap(),
        ));
    }
    
    fncode
}

/* figure out how big the stack needs to be for a given subprogram */
fn stackgen(fnbody: &str) -> String {
    /* just make it big enough for now */
    format!("  type stack[{}] = {{0}};\n", fnbody.len())
}

/* Reads the source and figures out what C code to generate and where */
fn parse(src: &str) -> String {
    let mut out = String::new();
    
    /* Add initial C headings that are required */
    out.push_str("#include <stdio.h>\n");
    out.push_str("#include <stdlib.h>\n");
    out.push_str("#include <math.h>\n");
    out.push_str("#include <string.h>\n\n");
    
    /* Create the union type that the stack will consist of */
    out.push_str("typedef union type\n\
    {\n  \
        FILE *f;\n  \
        char c;\n  \
        double d;\n  \
        char *s;\n  \
    } type;\n\n");
    
    /* Generate subprograms */
    let body = process_macros(src);
    let mut function_body = String::new();
    let mut function_name = String::new();
    let mut subprograms : HashMap<String, u8> = HashMap::new();
    let mut ignore_flag = false;
    let mut arg_searching = false;
    let mut parsing_function_body = false;
    let mut function_arity : u8 = 0;
    
    /* Figure out how to generate subprograms syntactically in C */
    for word in body.split_whitespace() {
        if ignore_flag {
            function_body.push_str(&format!(" {}", word));
            if word.starts_with('"') {
                ignore_flag = false;
            } else if word.ends_with('"') {
                ignore_flag = false;
            }
            
            continue;
        } else if word.starts_with('"') {
            if !word.ends_with('"') {
                ignore_flag = true;
            }
            
            function_body.push_str(&format!(" {}", word));
            continue;
        } else if word.starts_with('(') && word.ends_with(')') {
            out.push_str(&format!("type {}() {{\n", &word[1..word.len()-1])); 
            subprograms.insert(
                word[1..word.len()-1].to_string(), 
                function_arity
            );
            
            parsing_function_body = true;
        } else if word.starts_with('(') {
            out.push_str(&format!("type {}(", &word[1..]));
            function_name = word[1..].to_string();
            arg_searching = true;
        } else if arg_searching && !word.ends_with(')') {
            out.push_str(&format!("type {},", &word));
            function_arity += 1;
        } else if word.ends_with(')') && arg_searching {
            out.push_str(&format!("type {}) {{\n", &word[..word.len()-1]));
            subprograms.insert(function_name.clone(), function_arity+1);
            parsing_function_body = true;
            arg_searching = false;
        } else if word.starts_with(':') && parsing_function_body {
            out.push_str(&stackgen(&function_body));  
            out.push_str(&codegen(&function_body, subprograms.clone()));
            
            function_body = String::new();
            function_arity = 0;
            parsing_function_body = false;
            
            out.push_str("}\n\n");
        } else if parsing_function_body {
            function_body.push_str(&format!(" {}", word));
        }
    }
    
    /* Generate main function */
    out.push_str("int main(int argc, char **argv)\n{\n");
    
    let mut in_string = false;
    let mut toplevel_begin_index = 0;
    for i in 0..body.len() {
        let ch = body.as_bytes()[i];
        if ch == b'\"' && in_string == false {
            in_string = true;
        } else if ch == b'\"' && in_string == true {
            in_string = false;
        }
        
        if ch == b':' && in_string == false {
            toplevel_begin_index = i+1;
        }
    }
    
    /* Figure out where declarations of macros and functions end */
    let toplevel_body = &body[toplevel_begin_index..];
    out.push_str(&stackgen(&toplevel_body));
    out.push_str(&codegen(&toplevel_body, subprograms));
    out.push_str("  return 0;\n}");
    
    out
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let mut out_path = String::from("./zrpc_out");
    
    /* Argument handling, to be improved upon in the future */
    if args.len() < 2 {
        println!("Usage: zrpc <input file> <output file>");
        return Ok(())
    } else if args.len() > 2 {
        out_path = String::from(&args[2]);
    }
    
    let inp_path = String::from(&args[1]);
    
    if !metadata(inp_path.clone()).is_ok() {
        println!("zrpc: Cannot access input file \"{}\".", inp_path);
        return Ok(())
    }
    
    println!("Input file is {}, and output file is {}", inp_path, out_path);
    println!("-----------------------------------------------");
    
    let mut infile = File::open(inp_path)?;
    let mut prog_src = String::new();
    infile.read_to_string(&mut prog_src)?;
    
    let intermediate_src = parse(&prog_src); 
    println!("{}\n", intermediate_src);
    
    /* write the source to a file */
    let mut outfile = File::create("tmp.c")?;
    
    /* Compile automagically */
    Command::new("cc")
        .args(
            ["-lm", "-O2", "tmp.c", "-o", &format!("{}", out_path)]
        )
        .spawn()
        .expect("failed to run cc");
    
    outfile.write_all(&intermediate_src.into_bytes())?;
    
    Ok(())
}
