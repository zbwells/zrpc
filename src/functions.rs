/* SPDX-License Identifier: GPL-3.0-or-later */

/* 
 * Source file for zrpc programming language compiler.
 * Copyright (C) 2024 Zander B. Wells

 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <https://www.gnu.org/licenses/>.

 * zbwells <zbwells@protonmail.com>
 * Written as UVA Wise Honors program research project.
 */

/* Function handling code for the zrpc language.
 * deals with how functions are defined, and defines some built-ins
 */

use std::collections::HashMap;

pub struct Builtin {
    pub code: String,
    pub arity: u8,
    pub incptr: u8,
}

/* Make it so that the conversions happen automatically */
impl Builtin {
    fn new(code: &str, arity: u8, incptr: u8) -> Builtin {
        Builtin {
            code: code.to_string(),
            arity: arity as u8,
            incptr: incptr as u8,
        }
    }
}

/* Defines a list of builtins for the language */
pub fn get_builtins() -> HashMap<&'static str, Builtin> {
    let functions = HashMap::from([
        ("+", Builtin::new(
            "  stack[{count}].d = stack[{count}].d + stack[{count}+1].d;", 
            2, 1
        )),
        
        ("-", Builtin::new(
            "  stack[{count}].d = stack[{count}].d - stack[{count}+1].d;", 
            2, 1
        )),
        
        ("*", Builtin::new(
            "  stack[{count}].d = stack[{count}].d * stack[{count}+1].d;", 
            2, 1
        )),
        
        ("/", Builtin::new(
            "  stack[{count}].d = stack[{count}].d / stack[{count}+1].d;", 
            2, 1
        )),
        
        ("^", Builtin::new(
            "  stack[{count}].d = pow(stack[{count}].d, stack[{count}+1].d);", 
            2, 1
        )),
        
        ("%", Builtin::new(
            "  stack[{count}].d = \
            (int)stack[{count}].d % (int)stack[{count}+1].d;", 
            2, 1
        )),
        
        ("=", Builtin::new(
            "  printf(\"%.10g\", stack[{count}].d);", 
            1, 1
        )),
        
        ("flushinp", Builtin::new(
            "  { int c; while ( (c = getchar())\
            != \'\\n\' && c != EOF ) { } }\n",
            0, 0
        )),
        
        ("ascii", Builtin::new(
            "  stack[{count}].d = (int)stack[{count}].c;",
            1, 1
        )),
        
        ("atoc", Builtin::new(
            "  stack[{count}].c = (char)stack[{count}].d;",
            1, 1
        )),
        
        ("argc", Builtin::new(
            "  stack[{count}].d = argc;",
            0, 1
        )), 
        
        ("atof", Builtin::new(
            "  stack[{count}].d = atof(stack[{count}].s);",
            1, 1
        )),
        
        ("append", Builtin::new(
            "  stack[{count}].s = strcat(stack[{count}].s, \
            stack[{count}+1].s);\n",
            2, 1
        )),
        
        ("emptybuf", Builtin::new(
            "  stack[{count}].s = (char *)calloc(2048, 1);",
            0, 1
        )),
        
        ("strlen", Builtin::new(
            "  stack[{count}].d = strlen(stack[{count}-1].s);",
            0, 1
        )),
        
        ("strword", Builtin::new(
            "  stack[{count}].s = strtok(stack[{count}-1].s, \" \");",
            0, 1
        )),
        
        ("ref", Builtin::new(
            "  stack[{count}].c = \
            stack[{count}-1].s[(int)stack[{count}].d];",
            1, 1
        )),
        
        ("recurse", Builtin::new(
            "  main(argc, argv);\n",
            0, 0
        )), 
        
        ("head", Builtin::new(
            "  stack[{count}].c = stack[{count}-1].s[0];",
            0, 1
        )),
        
        ("tail", Builtin::new(
            "  stack[{count}].s = stack[{count}-1].s+1;",
            0, 1
        )),
        
        ("nl", Builtin::new("  putchar(\'\\n\');", 0, 0)),
        ("pop", Builtin::new("", 0, 0)),
        ("unpop", Builtin::new("", 0, 0)),
        ("print", Builtin::new(
            "  printf(\"%.10g\\n\", stack[{count}].d);", 
            0, 0
        )),
        
        ("printchr", Builtin::new("  putchar(stack[{count}].c);", 1, 1)),
        
        ("printstrs", Builtin::new(
            "  for (int x = 1; stack[{count}-x].s != \"\\0\"; x++) {\n    \
                 printf(\"%s\", stack[{count}-x].s);\n  \
               }\n\n", 0, 0
        )),
        
        ("printstr", Builtin::new(
            "  printf(\"%s\", stack[{count}].s);", 
            1, 1
        )),
        
        ("fprintstr", Builtin::new(
            "  fprintf(stack[{count}-1].f, \"%s\", \
            stack[{count}].s);",
            1, 1
        )),
        
        ("popstrs", Builtin::new("", 0, 0)),
        
        ("sqrt", Builtin::new(
            "  stack[{count}].d = sqrt(stack[{count}].d);", 1, 1
        )),
        
        ("pi", Builtin::new(
            "  stack[{}].d = M_PI;", 0, 1
        )),
        
        ("space", Builtin::new(
            "  stack[{count}].s = \" \";", 0, 1
        )),
        
        ("null", Builtin::new(
            "  stack[{count}].c = \'\\0\';", 0, 1
        )),
        
        ("streq", Builtin::new(
            "  stack[{count}].d = \
            !strcmp(stack[{count}].s, stack[{count}-1].s);",
            1, 1
        )),
        
        ("cheq", Builtin::new(
            "  stack[{count}].d = \
            stack[{count}].c == stack[{count}-1].c;",
            1, 1
        )),
        
        ("?{", Builtin::new(
            "\n  if (stack[{count}].d == stack[{count}+1].d) {", 2, 0
        )),
        
        ("!?{", Builtin::new(
            "\n  if (stack[{count}].d != stack[{count}+1].d) {", 2, 0
        )),
        
        ("<?{", Builtin::new(
            "\n  if (stack[{count}].d < stack[{count}+1].d) {", 2, 0
        )),
        
        (">?{", Builtin::new(
            "\n  if (stack[{count}].d > stack[{count}+1].d) {", 2, 0
        )),
        
        ("<-", Builtin::new(
            "  return stack[{count}];", 1, 1
        )),
        
        ("c", Builtin::new("  stack = {0};", 255, 0)),  
        
        ("d", Builtin::new("  stack[{count}] = stack[{count}-1];", 0, 1)),
        
        ("swap", Builtin::new(
            "  stack[{count}] = stack[{count}-1];\n  \
            stack[{count}-1] = stack[{count}-2];\n  \
            stack[{count}-2] = stack[{count}];",
            0, 0
        )),
        
        ("q", Builtin::new("  exit(0);", 0, 0)), 
        
        ("getnum", Builtin::new(
            "  stack[{count}+1].d = scanf(\"%lf\", &stack[{count}].d);\n  \
            { int c; while ( (c = getchar())\
            != \'\\n\' && c != EOF ) { } }",
            0, 1
        )),
        
        ("getarg", Builtin::new(
            "\n  if (argc < 2) {\n    \
              stack[{count}].s = \"\\0\";\n  \
            } else {\n    \
                argv++;\n   \
                stack[{count}].s = *argv;\n    \
                argc--;\n  \
            }\n\n",
            0, 1
        )),
        
        ("getstr", Builtin::new(
            "  stack[{count}].s = (char *)malloc(2048);\n  \
            stack[{count}].s = fgets(stack[{count}].s, 2048, stdin);\n  \
            stack[{count}].s[strcspn(stack[{count}].s, \"\\n\")] = 0;\n",
            0, 1
        )),
        
        ("getch", Builtin::new(
            "  stack[{count}].c = getchar();",
            0, 1
        )),
        
        ("rfopen", Builtin::new(
            "  stack[{count}].f = fopen(stack[{count}].s, \"r\");",
            1, 1
        )),
        
        ("wfopen", Builtin::new(
            "  stack[{count}].f = fopen(stack[{count}].s, \"w\");",
            1, 1
        )),
        
        ("afopen", Builtin::new(
            "  stack[{count}].f = fopen(stack[{count}].s, \"a+\");",
            1, 1
        )),
        
        ("eof", Builtin::new(
            "  stack[{count}].c = EOF;",
            0, 1
        )),
        
        ("fgetch", Builtin::new(
            "  stack[{count}].c = fgetc(stack[{count}-1].f);",
            0, 1
        )),
        
        ("}", Builtin::new("  }\n", 0, 0)),
    ]);
    
    functions
}

/* Generates code to push an object onto the stack */
pub fn push_onto_stack(obj: &str) -> String {
    let mut word = String::new();
    
    if obj.starts_with('_') {
        word.push_str(&format!("-{}", obj));
    } else if obj.starts_with('\'') {
        return format!("  stack[{{count}}].c = {};", obj);
    } else {
        word.push_str(&obj.to_string());
    }
    
    match &obj.parse::<f64>() {
        Ok(_) => format!("  stack[{{count}}].d = {};", word),
        Err(_) => format!("  stack[{{count}}] = {};", word),
    }
}

